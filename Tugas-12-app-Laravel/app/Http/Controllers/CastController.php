<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PhpParser\Node\Stmt\Return_;

class CastController extends Controller
{
    public function index(){
       // return view('cast.tampil');
       $cast = DB::table('cast')->get();
        //dd($cast);
       return view('cast.tampil', ['cast' => $cast]);
    }

    public function create(){
        return view('cast.tambah');
    }

    public function store(Request $request){
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        DB::table('cast')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio']
        ]);

        return redirect('/cast');
    }

    public function show($id){
        $detail = DB::table('cast')->find($id);

        return view('cast.detail', ['detail' => $detail ]);
    }

    public function edit($id){
        $detail = DB::table('cast')->find($id);

        return view('cast.edit', ['detail' => $detail ]);
    }

    public function update($id, Request $request){
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        DB::table('cast')
        ->where('id', $id)
        ->update(
        [
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio']
        ]);

        return redirect('/cast');
    }

    public function destroy($id){
        DB::table('cast')->where('id', '=', $id)->delete();

        return redirect('/cast');
    }
}