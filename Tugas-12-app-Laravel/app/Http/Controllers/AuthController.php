<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('register');
    }
    public function kirim(request $request){
        $namaDepan = $request['fname'];
        $namaBelakang = $request['lname'];

        return view('welcome',['namaDepan' => $namaDepan, 'namaBelakang' => $namaBelakang]);
    }
    

}
