@extends('layout.master')
@section('title')
    Halaman tambah cast
@endsection

@section('sub-title')
    tambah cast
@endsection

@section('content')

<form action="/cast" method="POST">
    @csrf
    <div class="form-group">
        <label for="nama">Nama:</label>
        <input type="text" class="form-control" name="nama" placeholder="Masukkan Nama">    
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label for="umur">Umur:</label>
        <input type="number" class="form-control" name="umur" placeholder="Masukkan Umur" >
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
     @enderror
    <div class="form-group">
        <label for="bio">Bio:</label>
        <textarea class="form-control"name="bio" cols="30" rows="10" placeholder="Masukkan bio"></textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
     @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection