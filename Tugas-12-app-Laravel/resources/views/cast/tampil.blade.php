@extends('layout.master')
@section('title')
    Halaman Data Cast
@endsection

@section('sub-title')
    tabel cast
@endsection

@section('content')
<a href="/cast/create" class="btn btn-primary btn-sm my-1">tambah</a>

<table class="table">
  <thead>
    <tr>
      <th>No</th>
      <th>Nama</th>
      <th>Umur</th>
      <th>Aksi</th>
    </tr>
  </thead>
  <tbody>
    @forelse ($cast as $key => $item)
    <tr>
      <td>{{$key + 1}}</td>
      <td>{{$item->nama}}</td>
      <td>{{$item->umur}}</td>  
      <td>
        <form action="/cast/{{$item->id}}" method="POST">
          @csrf
          @method('delete')
          <a href="/cast/{{$item->id}}" class="btn btn-success btn-sm">detail</a>
          <a href="/cast/{{$item->id}}/edit" class="btn btn-info btn-sm">Edit</a>
          <input type="submit" value="Delete" class="btn btn-danger btn-sm">
        </form>
      </td>
    </tr> 
    @empty
        <tr>
          <td>data kosong</td>
        </tr>
    @endforelse

  </tbody>
</table>
@endsection