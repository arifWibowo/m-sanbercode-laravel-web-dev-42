@extends('layout.master')
@section('title')
    Halaman Edit cast
@endsection

@section('sub-title')
    edit cast
@endsection

@section('content')

<form action="/cast/{{$detail->id}}" method="POST">
    @csrf
    @method('put')

    <div class="form-group">
        <label for="nama">Nama:</label>
        <input type="text" class="form-control" name="nama" value="{{$detail->nama}}">    
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label for="umur">Umur:</label>
        <input type="number" class="form-control" name="umur" value="{{$detail->umur}}" >
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
     @enderror
    <div class="form-group">
        <label for="bio">Bio:</label>
        <textarea class="form-control"name="bio" cols="30" rows="10">{{$detail->bio}}</textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
     @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection