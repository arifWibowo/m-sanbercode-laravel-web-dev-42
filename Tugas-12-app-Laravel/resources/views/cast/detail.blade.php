@extends('layout.master')
@section('title')
    Halaman Detail Cast
@endsection

@section('sub-title')
    detail
@endsection

@section('content')

<h1>{{$detail->nama}}</h1>
<h3>{{$detail->umur}} Th</h3>
<p>{{$detail->bio}}</p>

<a class="btn btn-dark" href="/cast" role="button">kembali</a>

@endsection